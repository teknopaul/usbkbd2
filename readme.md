# Using usbkbd2

To use usbkbd2 you need a second USB keyboard that can be dedicated as a midi keyboard.

N.B. dont try to use your main keyboard, or you will not be able to type letters!


## Build the binary

	sudo apt install libasound2-dev
	git clone https://gitlab.com/teknopaul/usbkbd2
	cd usbkbd2
	make
	sudo make install

Attach the USB keyboard to your computer and find the name used by Linux for the input device.

Running 

	usbkbd2

With no arguments prints the attached keyboards, if you are lucky, it may be obvious which is the correct keyboard name.

If its not obvious use `lsusb`, `xinput` and the symlinks in `/dev/input/by-id`, plug and unplug the device to see which links are created.

## Example

In this case I'm using a Kensington USB number pad as a midi device, its not obvious from the symlinks which is the correct device but 
by unplugging and plulgging the device back in its possible to see which link is newly created.

	teknopaul@abox:~/gitlab_workspace/usbkbd2$ ll /dev/input/by-id
	total 0
	drwxr-xr-x 2 root root 200 Jan  2 03:33 .
	drwxr-xr-x 4 root root 580 Jan  2 03:33 ..
	lrwxrwxrwx 1 root root  10 Jan  2 03:33 usb-05a4_9865-event-kbd -> ../event20
	lrwxrwxrwx 1 root root  10 Jan  2 01:45 usb-Compx_2.4G_Receiver-event-if02 -> ../event11
	lrwxrwxrwx 1 root root   9 Jan  2 01:45 usb-Compx_2.4G_Receiver-event-kbd -> ../event9
	lrwxrwxrwx 1 root root  10 Jan  2 01:45 usb-Compx_2.4G_Receiver-if01-event-mouse -> ../event10
	lrwxrwxrwx 1 root root   9 Jan  2 01:45 usb-Compx_2.4G_Receiver-if01-mouse -> ../mouse1
	lrwxrwxrwx 1 root root   9 Dec 14 09:08 usb-Generic_Chicony_USB_2.0_Camera_200901010001-event-if00 -> ../event6
	lrwxrwxrwx 1 root root   9 Dec 14 09:08 usb-Logitech_USB_Receiver-if02-event-mouse -> ../event5
	lrwxrwxrwx 1 root root   9 Dec 14 09:08 usb-Logitech_USB_Receiver-if02-mouse -> ../mouse0

`usb-05a4_9865-event-kbd` is the name of the device that usbkbd2 needs.

Now running `usbkbd2` will translate key presses into midi notes, the `-d` option is used to ensure that key presses are not seen by non-midi applications.

	sudo usbkbd2 -v -d -e 80 -k usb-05a4_9865-event-kbd

The `-e` option sets the velocity (volume) of the midi notes generated.

You need to detatch the keyboard from X or it will send key strokes to the active application.


`xinput` shows the same id, in this case all there is to go on is the USBID `HID 05a4:9865`.

	teknopaul@abox:~/gitlab_workspace/usbkbd2$ xinput
	⎡ Virtual core pointer                    	id=2	[master pointer  (3)]
	⎜   ↳ Virtual core XTEST pointer              	id=4	[slave  pointer  (2)]
	⎜   ↳ Logitech MX Anywhere 2                  	id=10	[slave  pointer  (2)]
	⎜   ↳ SynPS/2 Synaptics TouchPad              	id=17	[slave  pointer  (2)]
	⎜   ↳ Compx 2.4G Receiver Consumer Control    	id=13	[slave  pointer  (2)]
	⎜   ↳ Compx 2.4G Receiver                     	id=19	[slave  pointer  (2)]
	⎣ Virtual core keyboard                   	id=3	[master keyboard (2)]
	    ↳ Virtual core XTEST keyboard             	id=5	[slave  keyboard (3)]
	    ↳ Power Button                            	id=6	[slave  keyboard (3)]
	    ↳ Video Bus                               	id=7	[slave  keyboard (3)]
	    ↳ Power Button                            	id=8	[slave  keyboard (3)]
	    ↳ Sleep Button                            	id=9	[slave  keyboard (3)]
	    ↳ Chicony USB 2.0 Camera: Chicony         	id=11	[slave  keyboard (3)]
	    ↳ AT Translated Set 2 keyboard            	id=16	[slave  keyboard (3)]
	    ↳ Logitech MX Anywhere 2                  	id=18	[slave  keyboard (3)]
	    ↳ Compx 2.4G Receiver                     	id=12	[slave  keyboard (3)]
	    ↳ Compx 2.4G Receiver Consumer Control    	id=14	[slave  keyboard (3)]
	    ↳ Compx 2.4G Receiver System Control      	id=15	[slave  keyboard (3)]
	    ↳ HID 05a4:9865                           	id=20	[slave  keyboard (3)]

To detach the device so that it does not generate X events, i.e. when you hit the keys you dont type letters in the active applications.

	xinput float 20

Running `xinput` again shows that the device is floating.

	teknopaul@sbox:~/gitlab_workspace/usbkbd2$ xinput
	⎡ Virtual core pointer                    	id=2	[master pointer  (3)]
	⎜   ↳ Virtual core XTEST pointer              	id=4	[slave  pointer  (2)]
	⎜   ↳ Logitech MX Anywhere 2                  	id=10	[slave  pointer  (2)]
	⎜   ↳ SynPS/2 Synaptics TouchPad              	id=17	[slave  pointer  (2)]
	⎜   ↳ Compx 2.4G Receiver Consumer Control    	id=13	[slave  pointer  (2)]
	⎜   ↳ Compx 2.4G Receiver                     	id=19	[slave  pointer  (2)]
	⎣ Virtual core keyboard                   	id=3	[master keyboard (2)]
	    ↳ Virtual core XTEST keyboard             	id=5	[slave  keyboard (3)]
	    ↳ Power Button                            	id=6	[slave  keyboard (3)]
	    ↳ Video Bus                               	id=7	[slave  keyboard (3)]
	    ↳ Power Button                            	id=8	[slave  keyboard (3)]
	    ↳ Sleep Button                            	id=9	[slave  keyboard (3)]
	    ↳ Chicony USB 2.0 Camera: Chicony         	id=11	[slave  keyboard (3)]
	    ↳ AT Translated Set 2 keyboard            	id=16	[slave  keyboard (3)]
	    ↳ Logitech MX Anywhere 2                  	id=18	[slave  keyboard (3)]
	    ↳ Compx 2.4G Receiver                     	id=12	[slave  keyboard (3)]
	    ↳ Compx 2.4G Receiver Consumer Control    	id=14	[slave  keyboard (3)]
	    ↳ Compx 2.4G Receiver System Control      	id=15	[slave  keyboard (3)]
	∼ HID 05a4:9865                           	id=20	[floating slave]

To reattach the device

	xinput reattach 20 3

Or unplug it and plug it back in again.

Ids may change depending on the ording in which USB devices are configured. 
Once the device name is known you can script detaching the keyboard as follows.

	xinput float $(xinput list --id-only 'HID 05a4:9865')
	sudo usbkbd2 -v -d -e 80 -k usb-05a4_9865-event-kbd


## Midi mapping

usbkbd2 converts key presses to midi note on events, each key is given a different note, the number chosen is based on the key id used by Linux input events.

Running `usbkbd2 -v`  prints the midi notes that usbkbd2 has generated from key presses.

You may need these to midimap keys on the keyboard to instruments or other software inputs, e.g actions in LMMS.

