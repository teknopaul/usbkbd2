

all: src/usbkbd2.c
	mkdir -p target
	gcc -Wall -Werror -o target/usbkbd2 src/usbkbd2.c -lasound


.PHONY: clean install uninstall deb test

test:
#        mkdir -p target/
#        gcc -Wall -Isrc/c -o target/libcdj_test src/test/test_libcdj.c libcdj.a
#        ./target/libcdj_test

clean:
	rm -f *.so *.a *.o target/usbkbd2
	rm -rf target/

install:
	cp target/usbkbd2 /usr/bin/

uninstall:
	rm /usr/bin/usbkbd2

deb:
	sudo deploy/build-deb.sh

