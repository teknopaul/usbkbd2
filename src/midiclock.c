#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <linux/input.h>
#include <sys/stat.h>
#include <ctype.h>
#include <sys/select.h>
//#include <vector>
#include <alsa/asoundlib.h>



int MIDI_BUFFER_SIZE = 4; // only note on and off
int HEX = 0;
int VERBOSE = 0;

snd_seq_t * alsa_seq = 0;
int alsa_port = 0;

static int send_midi_start(unsigned char channel);
static int send_midi_stop(unsigned char channel);
static int send_midi_clock(int code, unsigned char channel);
static int setup_alsa(char * display_name);

static void usage()
{
    printf("options:\n");
    printf("         -c - midi channel\n");
    printf("         -n - change the name of the device in alsa\n");
    printf("         -h - display this text\n");
    printf("         -v - verbose, print midi events\n");
    printf("         -0 - note values printed in hexidecimal\n");
    exit(1);
}

int main(int argc, char **argv)
{
    long int v, ch;
    long id;
    unsigned char velocity = 100;
    unsigned char channel = 0;
    char* display_name = NULL;
    char* keyb_name = NULL;
    int auto_detatch = 0;

    // parse command line
    if (argc == 1) usage();

    int c;
    while ( ( c = getopt(argc, argv, "k:e:n:c:hxvd0") ) != EOF) {
        switch (c) {
            default:
            case 'h':
                usage();
                break;
            case 'x':
                dump_xorg_conf();
                break;
            case 'k': 
                keyb_name = optarg;
                break;
            case 'n': 
                display_name = optarg;
                break;
            case 'c': 
                ch = strtol(optarg, NULL, 10);
                if (ch > 0 && ch <= 16) {
                    channel = (unsigned char)(ch - 1);
                } else {
                    printf("invalid channel '%li'\n", ch);
                    exit(1);
                }
                break;
            case 'e': {
                v = strtol(optarg, NULL, 10);
                if (v > 0 && v < 255) {
                    velocity = (unsigned char)v;
                } else {
                    printf("invalid velocity '%li'\n", v);
                    exit(1);
                }
                break;
            }
            case 'd': 
                auto_detatch = 1;
                break;
            case 'v': 
                VERBOSE = 1;
                break;
            case '0': 
                HEX = 1;
                break;
        }
    }
    
    if ( ! keyb_name) usage();
    if ( ! display_name) display_name = keyb_name;

    char device[1024];
    memset(device, 0, 1024);
    strcat(device, "/dev/input/by-id/");
    strncat(device, keyb_name, 1000);

    struct stat stat_s;
    
    int stat_ret = stat(device, &stat_s);
    
    if (stat_ret != 0) {
        fprintf(stderr, "Unable to stat file '%s'\n", device);
        return 1;
    }
    

    if (auto_detatch) {
        // resolve symlink, path indicates id used by xinput
        char* path = realpath(device, NULL);
        char* p = path;
        if (p) {
            // skip all the letters in  /dev/input/event11
            while (*p && ! isdigit(*p)) p++;
            
            id = strtol(p, &p, 10);
            free(path);
            
            char xinput_cli[1024];
            sprintf(xinput_cli, "xinput float %li", id);
            if ( system(xinput_cli) == 0 ) {
                printf("Detatched input device %li\n", id);
            }
        }
    }

    // open file to read
    int in = open(device, O_RDONLY);
    if (in == -1) {
        fprintf(stderr, "Unable to open '%s'\n", device);
        if ( getuid() ) fprintf(stderr, "Try root user/sudo\n");
        return 3;
    }
    
    int ret = setup_alsa(display_name);
    if (ret != 0) {
        return ret;
    }
        
    struct input_event ev[64];
    
    int size = sizeof(struct input_event);
    int red = 0;
    while (1) {
        if ((red = read(in, ev, size * 64)) < size) {
            printf("Read error, USB detatched?\n");
            exit(1);
        }
    
        int value = ev[0].value;
        
        //printf ("type %x\n", (ev[1].type));
        //printf ("code %x\n", (ev[1].code));
        //printf ("value %x\n", (ev[1].value));
    
        if (value != ' ' && ev[1].value == 1 && ev[1].type == 1) { // key on
            if (VERBOSE) {
                if (HEX) printf ("Note on %x\n", ev[1].code );
                else printf ("Note on %i\n", ev[1].code );
            }
            send_midi_on(ev[1].code, channel, velocity);
        }

        if (value != ' ' && ev[1].value == 0 && ev[1].type == 1) { // key off
            if (VERBOSE) {
                if (HEX) printf ("Note off %x\n", ev[1].code );
                else printf ("Note off %i\n", ev[1].code );
            }
            send_midi_off(ev[1].code, channel);
        }

    }
    
    return 0;
}


static int send_midi_start(int code, unsigned char channel, unsigned char velocity)
{
    snd_seq_event_t ev;
    snd_seq_ev_clear(&ev);
    snd_seq_ev_set_source(&ev, alsa_port);
    snd_seq_ev_set_subs(&ev);
    snd_seq_ev_set_direct(&ev);
    snd_seq_ev_set_noteon(&ev, channel, code, velocity);

    snd_seq_event_output_direct(alsa_seq, &ev);
    return 0;
}

static int send_midi_stop(int code, unsigned char channel)
{
    snd_seq_event_t ev;
    snd_seq_ev_clear(&ev);
    snd_seq_ev_set_source(&ev, alsa_port);
    snd_seq_ev_set_subs(&ev);
    snd_seq_ev_set_direct(&ev);
    snd_seq_ev_set_noteoff(&ev, channel, code, 0);
    snd_seq_event_output_direct(alsa_seq, &ev);
    return 0;
}


static int send_midi_stop(int code, unsigned char channel)
{
    snd_seq_event_t ev;
    snd_seq_ev_clear(&ev);
    snd_seq_ev_set_source(&ev, alsa_port);
    snd_seq_ev_set_subs(&ev);
    snd_seq_ev_set_direct(&ev);
    snd_seq_ev_set_noteoff(&ev, channel, code, 0);
    snd_seq_event_output_direct(alsa_seq, &ev);
    return 0;
}

static int setup_alsa(char * display_name)
{
  
    // Setup Alsa

    int alsa_err;
    if ( ( alsa_err = snd_seq_open(&alsa_seq, "default", SND_SEQ_OPEN_DUPLEX, SND_SEQ_NONBLOCK)) <  0) {
        fprintf(stderr, "Could not init ALSA sequencer: %s\n", snd_strerror(alsa_err));
        alsa_seq = 0;
        return 1;
    } else {
        snd_seq_set_client_name(alsa_seq, display_name);
        printf ("Initialised ALSA sequencer: '%s'\n", display_name);
    }

    alsa_port = snd_seq_create_simple_port(alsa_seq, "clock",
            SND_SEQ_PORT_CAP_WRITE|SND_SEQ_PORT_CAP_SUBS_WRITE|SND_SEQ_PORT_CAP_READ|SND_SEQ_PORT_CAP_SUBS_READ,
            SND_SEQ_PORT_TYPE_APPLICATION);
        
    if (alsa_port < 0) {
        fprintf(stderr, "Could not create ALSA sequencer port\n");
        return 1;
    }

    return 0;
}

